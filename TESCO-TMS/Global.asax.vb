﻿Imports System.Web.Optimization

Public Class Global_asax
    Inherits HttpApplication

    Sub Application_Start(sender As Object, e As EventArgs)
        ' Fires when the application is started

    End Sub

    Sub Session_End()
        GC.Collect()
    End Sub
End Class