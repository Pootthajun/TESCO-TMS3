﻿var agent = navigator.userAgent.toLowerCase();
var regGG = /chrome\/([\d]+)/gi;
var device = agent.indexOf("chrome") > 0 && agent.indexOf("edge") < 0 && agent.indexOf("Windows NT 6.1; Trident/7.0;") < 0;
var version = regGG.exec(agent)[1] > 58;
var collectAction = {
    bootstrap: function () {
        if (device)
            this.isFlashNormal();
    },
    isFlashInstall: function () {
        var flashStatus = -1;
        if (document.all) {
            try {
                var swf1 = new ActiveXObject('ShockwaveFlash.ShockwaveFlash');
                flashStatus = 1;
            } catch (e) {
                flashStatus = 0;
            }
        } else {
            try {
                var swf2 = navigator.plugins['Shockwave Flash'];
                if (swf2 == undefined) {
                    flashStatus = 0;
                } else {
                    flashStatus = 1;
                }
            } catch (e) {
                flashStatus = 0;
            }
        }
        return flashStatus;
    },
    isFlashNormal: function () {
        window.isflash = collectAction.isFlashInstall();
        window.flash_normal = -1;
        console.log(!window.isflash && version);
        if (!window.isflash && version) {

            window.location.href = '//www.adobe.com/go/getflash';
        }
        //$('#J_show_tips2').show();
    }
};

var ua = window.navigator.userAgent,
    isAndroid = /Android\s+([\d.]+)/.test(ua),
    isIos = /(?:iPad|iPhone).*OS\s([\d_]+)/.test(ua),
    isMobile;
(isAndroid || isIos) ? (isMobile = true) : (isMobile = false);
isMobile ? null : collectAction.bootstrap();

function eventsAction() {
    /*
    $('.cancle').on("click",function(){
        $('#J_show_tips1').hide();
    });

    $('#reflash').on("click",function(){
        location.reload();
    });

    $('button').on('mouseover',function(){
        $(this).css('cursor',"pointer");
    }).on("mouseout",function(){
        $(this).css('cursor',"normal");
    });

    $('.no-respon-btn').on('click', function(){
        var tip1 = $('#J_show_tips1'),
            tip2 = $('#J_show_tips2');
        tip2.fadeOut();
        tip1.fadeIn();
    });
    */
}
eventsAction();