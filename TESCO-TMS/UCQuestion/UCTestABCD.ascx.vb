﻿Imports System.Data.SqlClient
Imports LinqDB.ConnectDB
Imports LinqDB.TABLE
Public Class UCTestABCD
    Inherits System.Web.UI.UserControl
    Public ReadOnly Property UserData As UserProfileData
        Get
            Return Session("UserData")
        End Get
    End Property

    Public Property answerDB As String
        Get
            Return ViewState("answerDB")
        End Get
        Set(value As String)
            ViewState("answerDB") = value
        End Set
    End Property

    Public Property choiceDB As String
        Get
            Return ViewState("choiceDB")
        End Get
        Set(value As String)
            ViewState("choiceDB") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            ckbA.Attributes("onchange") = "preventDuplicateChecked('A')"
            ckbB.Attributes("onchange") = "preventDuplicateChecked('B')"
            ckbC.Attributes("onchange") = "preventDuplicateChecked('C')"
            ckbD.Attributes("onchange") = "preventDuplicateChecked('D')"
        End If

    End Sub


    Public Sub setTestQuestionABCD(test_id As String, question_no As Double, QuestionCount As Integer, ShowAnswer As String)
        txtTestID.Text = test_id
        txtShowAnswer.Text = ShowAnswer
        Try
            Me.txtQuestion_no.Text = question_no.ToString
            Dim dt As DataTable = GetTestQuestion(test_id, question_no)

            txtQuestionID.Text = dt.Rows(0)("id")
            pnlQuestionABCD.Visible = True
            Dim str As String = ""
            If (dt.Rows.Count > 0) Then
                Me.lblQNumber.Text = "ข้อ " + question_no.ToString + "/" + QuestionCount.ToString
                Me.lblQDetail.Text = dt.Rows(0)("question_title") & ""
                txtIconURL.Text = dt.Rows(0)("icon_url").ToString
                If Convert.IsDBNull(dt.Rows(0)("weight")) = False Then txtWeight.Text = dt.Rows(0)("weight")
                txtIsRandomAns.Text = dt.Rows(0)("is_random_answer")

                Dim srt2 As String
                If txtIconURL.Text <> "" Then
                    srt2 = txtIconURL.Text.Substring(txtIconURL.Text.Length - 3)
                End If
                'If srt2 = "mp4" Then
                '    If Convert.IsDBNull(dt.Rows(0)("icon_url")) = False Then
                '        links.Visible = False

                '        myVideo.Visible = True
                '        myVideo.Attributes.Add("src", dt.Rows(0)("icon_url"))
                '    End If
                'Else
                If dt.Rows(0)("icon_url") & "" <> "" Then
                    links.InnerHtml = "<a href='" & dt.Rows(0)("icon_url") & "' title='' data-gallery='' >"
                    links.InnerHtml += "    <img  src='" & dt.Rows(0)("icon_url") & "' style='width:200px;height:200px' />"
                    links.InnerHtml += "</a>"
                End If
                'End If

                answerDB = dt.Rows(0)("answer").ToString
                choiceDB = dt.Rows(0)("choice").ToString
                Dim tmpAnswer() As String = Split(answerDB, "##")
                Dim tmpChoice() As String = Split(choiceDB, "##")

                If tmpChoice.Length = 4 And tmpAnswer.Length = 4 Then
                    lblA.InnerText = tmpChoice(0)
                    lblB.InnerText = tmpChoice(1)
                    lblC.InnerText = tmpChoice(2)
                    lblD.InnerText = tmpChoice(3)

                    Dim i As Integer = 0
                    For Each ans As String In tmpAnswer
                        If ans.ToLower = "true" Then
                            txtCorrectAnswer.Text = i
                            txtCorrectChoice.Text = tmpChoice(i)
                        End If
                        i += 1
                    Next
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnAns_ServerClick(sender As Object, e As EventArgs) Handles btnAns.ServerClick
        'LogFileBL.LogTrans(UserData.LoginHistoryID, "คลิกปุ่มตอบ")

        Dim AnswerText As String = ""
        If ValidateData() = True Then
            Dim isCorrect As Boolean = False
            Dim AnswerChoice As Integer = -1
            If ckbA.Checked = True Then
                AnswerChoice = 0
                AnswerText = lblA.InnerText
                If txtCorrectAnswer.Text = 0 Then
                    isCorrect = True
                End If
            ElseIf ckbB.Checked Then
                AnswerChoice = 1
                AnswerText = lblB.InnerText
                If txtCorrectAnswer.Text = 1 Then
                    isCorrect = True
                End If
            ElseIf ckbC.Checked = True Then
                AnswerChoice = 2
                AnswerText = lblC.InnerText
                If txtCorrectAnswer.Text = 2 Then
                    isCorrect = True
                End If
            ElseIf ckbD.Checked = True Then
                AnswerChoice = 3
                AnswerText = lblD.InnerText
                If txtCorrectAnswer.Text = 3 Then
                    isCorrect = True
                End If
            End If

            Dim TimeSpen As Integer = DateDiff(DateInterval.Second, Convert.ToDateTime(Session("teststarttime")), DateTime.Now)


            Dim trans As New TransactionDB
            'Dim qHisID As Long = SaveQuestionHis(UserData.UserName, trans, Session("TestingHisID"), txtQuestion_no.Text, lblQDetail.Text, txtIconURL.Text, txtWeight.Text, "abcd", IIf(txtIsRandomAns.Text = "Y", True, False), AnswerChoice, txtCorrectAnswer.Text)
            Dim qHisID As Long = SaveQuestionHis_New(UserData.UserName, Session("TestingHisID"), txtQuestion_no.Text, lblQDetail.Text, txtIconURL.Text, txtWeight.Text, "abcd", IIf(txtIsRandomAns.Text = "Y", True, False), choiceDB, answerDB, AnswerChoice, txtCorrectAnswer.Text)
            SaveTestAnswer_New(UserData.UserName, txtTestID.Text, txtQuestionID.Text, TimeSpen, AnswerChoice, AnswerText, IIf(isCorrect = True, "Y", "N"), Session("TestingHisID"), qHisID)
            'If qHisID > 0 Then
            '    Dim ret As ExecuteDataInfo = SaveTestAnswer(UserData.UserName, trans, txtTestID.Text, txtQuestionID.Text, TimeSpen, AnswerChoice, IIf(isCorrect = True, "Y", "N"), Session("TestingHisID"), qHisID)
            '    If ret.IsSuccess = True Then
            '        trans.CommitTransaction()
            '    Else
            '        trans.RollbackTransaction()
            '    End If

            'Else
            '    trans.RollbackTransaction()
            'End If

            '---------------Save Question---------------


            Dim LogMsg As String = "ตอบคำถาม " & lblQNumber.Text & " " & lblQDetail.Text
            If isCorrect = True Then
                lblDialogHead.Text = "ยินดีด้วย"
                litAnsDetail.Text = "<h2>คุณตอบถูก</h2>"
                LogMsg += " ตอบถูก"
            Else
                lblDialogHead.Text = "คุณตอบผิด"
                divHeader.Attributes.Remove("style")
                divHeader.Attributes.Add("style", "background:red")

                litAnsDetail.Text = "<font color='#019b79'><h4>คำตอบที่ถูกคือ<h4></font>"
                litAnsDetail.Text += "<font color='#019b79'><h4>" + txtCorrectChoice.Text + "</h4></font>"
                LogMsg += " ตอบผิด"
            End If

            'LogFileBL.LogTrans(UserData.LoginHistoryID, LogMsg)
            If txtShowAnswer.Text = "Y" Then
                pnlAnsResult.Visible = True
            Else
                Response.Redirect("frmSelectQuestionTest.aspx?id=" & txtTestID.Text & "&q_id=" & (Convert.ToInt16(txtQuestion_no.Text) + 1))
            End If
        End If
    End Sub

    Private Function ValidateData() As Boolean
        If ckbA.Checked = False And ckbB.Checked = False And ckbC.Checked = False And ckbD.Checked = False Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาเลือกคำตอบ');", True)
            Return False
        End If

        Return True
    End Function

    Private Sub btnCloseDialog_Click(sender As Object, e As EventArgs) Handles btnCloseDialog.Click
        'LogFileBL.LogTrans(UserData.LoginHistoryID, "คลิกปุ่ม ปิด")
        pnlAnsResult.Visible = False
    End Sub

    Private Sub btnNext_Click(sender As Object, e As EventArgs) Handles btnNext.Click
        'LogFileBL.LogTrans(UserData.LoginHistoryID, "คลิกปุ่ม ต่อไป")
        Response.Redirect("frmSelectQuestionTest.aspx?id=" & txtTestID.Text & "&q_id=" & (Convert.ToInt16(txtQuestion_no.Text) + 1))
    End Sub
End Class